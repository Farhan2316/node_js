import multPI from "./mult";


export default class addPI{
    
    constructor(n){
        this.n = n;
    }
    
    calculate(){
        let res=this.n+Math.PI;
        return (this.n && res<(300))? (console.log(this.n+' + π = '+ res) +  new multPI(res).calculate()) : this.n;
}

}