import addPI from "./add";

export default class multPI{
    
    constructor(n){
        this.n = n;
    }
    
    calculate(){

        var res = this.n*Math.PI;
         return (this.n && res<(300))? (console.log(this.n+' * π = '+res) +  new addPI(res).calculate()) : this.n;
    
}
}